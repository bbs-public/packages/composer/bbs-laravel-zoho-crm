<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Enums\DataCenterEnvironment;
use BBSLab\ZohoCRM\Enums\DataCenterLocation;
use BBSLab\ZohoCRM\Enums\StoreType;
use com\zoho\api\logger\Levels;

return [
    /*
    |--------------------------------------------------------------------------
    | Client keys
    |--------------------------------------------------------------------------
    |
    | Contains all the relevant data that will be used for authentication.
    |
    */
    'client' => [
        'client_id' => env('ZOHO_CLIENT_ID', '1000.792BLKKKWINUVT5VYBIW80X2W0DFXC'),
        'client_secret' => env('ZOHO_CLIENT_SECRET', 'f9802456eb7bccf399c37a0b3fef9c0db30edfa2f0'),
        'redirect_url' => env('ZOHO_REDIRECT_URL', 'http://127.0.0.1:8000/zoho/callback'),
    ],

    /*
    |--------------------------------------------------------------------------
    | OAuth config
    |--------------------------------------------------------------------------
    |
    | Can be used to specify the host name used for authentication requests
    | Scopes can be defined as well in the array below.
    |
    | Please note that requests will fail if the generated token does not have
    | the required scopes.
    |
    */
    'oauth' => [
        'host' => env('ZOHO_ACCOUNTS_HOST', 'accounts.zoho.eu'),

        'scopes' => [
            'ZohoCRM.modules.ALL',
            'ZohoCRM.settings.ALL',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Email
    |--------------------------------------------------------------------------
    |
    | The email that will be used for the authentication process
    |
    */
    'email' => env('ZOHO_USER_EMAIL', 'guillaume.tonet@neofa.com'),

    /*
    |--------------------------------------------------------------------------
    | Token persistence
    |--------------------------------------------------------------------------
    |
    | Can be used to specify the store used to persist oauth tokens.
    |
    | Available : database
    |
    | See : config('zohocrm.stores']
    |
    | todo: add file and custom stores
    |
    */
    'persistence' => StoreType::DATABASE(),

    /*
    |--------------------------------------------------------------------------
    | Logger
    |--------------------------------------------------------------------------
    |
    | This holds the level of reports logging, and the absolute path to the log
    | file in storage.
    |
    | Available levels: ALL, TRACE, DEBUG, INFO, WARNING, ERROR, FATAL
    |
    */
    'logger' => [
        'level' => Levels::ALL,
        'path' => storage_path('logs/zoho.log'),
    ],

    /*
    |--------------------------------------------------------------------------
    | Environment
    |--------------------------------------------------------------------------
    |
    | Sets the location and environment for the data center used for the
    | subsequent requests to the API.
    |
    | Available locations: EU,US,CN,INDIA,AU
    | Available environments: PRODUCTION,DEVELOPER,SANDBOX
    |
    */
    'data_center' => [
        'location' => DataCenterLocation::EU(),
        'environment' => DataCenterEnvironment::PRODUCTION(),
    ],


    /*
    |--------------------------------------------------------------------------
    | Stores
    |--------------------------------------------------------------------------
    |
    | Defines the options and settings for the different available stores
    |
    */
    'stores' => [
        'database' => [
            'table_name' => 'zoho_oauth_tokens',
        ],
    ],

    /*
    |--------------------------------------------------------------------------
    | Resources path
    |--------------------------------------------------------------------------
    |
    | Sets the path where the resoources of the retrieved records should
    | be stored
    |
    */

    'resources' => [
        'path' => storage_path('public/zoho/resources'),
    ],
];