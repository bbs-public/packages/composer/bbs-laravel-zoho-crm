<?php

declare(strict_types=1);

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class() extends Migration {
    protected string $tableName;

    public function __construct()
    {
        $this->tableName = config('zohocrm.stores.database.table_name', 'zoho_tokens');
    }

    public function up(): void
    {
        Schema::create($this->tableName, function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('client_id')->nullable();
            $table->string('access_token')->nullable();
            $table->string('refresh_token')->nullable();
            $table->string('grant_token')->nullable();
            $table->dateTime('expires_at')->nullable();
            $table->timestamps();

            $table->unique(['email', 'client_id']);
        });
    }

    public function down(): void
    {
        Schema::dropIfExists($this->tableName);
    }
};