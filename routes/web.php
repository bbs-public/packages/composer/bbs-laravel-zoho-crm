<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Http\Controllers\OAuthController;
use Illuminate\Support\Facades\Route;

Route::get('zoho/callback', [OAuthController::class, 'callback'])
    ->name('zohocrm.callback');
