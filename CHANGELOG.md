# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.6](https://github.com/crezra/laravel-zoho-crm/compare/v0.1.5...v0.1.6) (2021-10-15)


### Features

* **resources:** added commands to create and clear resource directories ([a8c897e](https://github.com/crezra/laravel-zoho-crm/commit/a8c897e5a7bc8c52427e0d98a23c254c5e14c0c5))

### [0.1.5](https://github.com/crezra/laravel-zoho-crm/compare/v0.1.4...v0.1.5) (2021-10-11)


### Features

* **fields:** added additional data to the fields command output ([1c3db25](https://github.com/crezra/laravel-zoho-crm/commit/1c3db25878d9be52428614ae0646fd7edf143342))

### [0.1.4](https://github.com/crezra/laravel-zoho-crm/compare/v0.1.3...v0.1.4) (2021-10-11)


### Bug Fixes

* **resources:** added a method to ensure resources dir exists ([877c0cf](https://github.com/crezra/laravel-zoho-crm/commit/877c0cf22a5bb923c683cba8141e8ebf1bb4f58a))

### [0.1.3](https://github.com/crezra/laravel-zoho-crm/compare/v0.1.2...v0.1.3) (2021-10-05)


### Features

* **exception:** removed exception catching ([2052163](https://github.com/crezra/laravel-zoho-crm/commit/20521637bf7fb20a6ec1771bfab6960cab41fae3))

### [0.1.2](https://github.com/crezra/laravel-zoho-crm/compare/v0.1.1...v0.1.2) (2021-10-04)

### 0.1.1 (2021-10-04)


### Features

* added modules: Leads, Contacts ([5a6c31e](https://github.com/crezra/laravel-zoho-crm/commit/5a6c31e858acc396e3f5cedbeca4a8347241b7d4))


### Bug Fixes

* fixing ci ([970ba7a](https://github.com/crezra/laravel-zoho-crm/commit/970ba7a5bad6c0330ade54bdada2193a157c3d76))
