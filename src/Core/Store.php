<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Core;

use BBSLab\ZohoCRM\Enums\StoreType;
use BBSLab\ZohoCRM\Stores\EloquentStore;

class Store
{
    protected EloquentStore $store;

    protected StoreType $persistence;

    public function __construct()
    {
        $this->persistence = config('zohocrm.persistence', StoreType::DATABASE());

        switch ($this->persistence->value) {
            default:
                $this->store = new EloquentStore();
                break;
        }
    }

    public static function instance(): EloquentStore
    {
        return (new self())->store;
    }
}