<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Core;

use com\zoho\crm\api\UserSignature as ZohoSDKUserSignature;

class UserSignature
{
    public ZohoSDKUserSignature $user;

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public function __construct(?string $email = null)
    {
        $this->user = new ZohoSDKUserSignature($email ?? config('zohocrm.email'));
    }

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public static function instance(?string $email = null): ZohoSDKUserSignature
    {
        return (new self($email))->user;
    }
}