<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Core;

use BBSLab\ZohoCRM\Enums\DataCenterEnvironment;
use BBSLab\ZohoCRM\Enums\DataCenterLocation;
use com\zoho\crm\api\dc\AUDataCenter;
use com\zoho\crm\api\dc\CNDataCenter;
use com\zoho\crm\api\dc\EUDataCenter;
use com\zoho\crm\api\dc\INDataCenter;
use com\zoho\crm\api\dc\USDataCenter;
use ReflectionClass;

class Environment
{
    public DataCenterLocation $dataCenterLocation;
    public DataCenterEnvironment $dataCenterEnvironment;

    public function __construct(
        ?DataCenterLocation $dataCenterLocation = null,
        ?DataCenterEnvironment $dataCenterEnvironment = null
    ) {
        $this->dataCenterLocation = $dataCenterLocation ?? config(
                'zohocrm.data_center.location',
                DataCenterLocation::EU(),
            );

        $this->dataCenterEnvironment = $dataCenterEnvironment ?? config(
                'zohocrm.data_center.environment',
                DataCenterEnvironment::PRODUCTION(),
            );
    }

    /**
     * @throws \ReflectionException
     */
    public function setup()
    {
        switch ($this->dataCenterLocation->value) {
            case DataCenterLocation::US:
                $dataCenterClass = USDataCenter::class;
                break;
            case DataCenterLocation::INDIA:
                $dataCenterClass = INDataCenter::class;
                break;
            case DataCenterLocation::CN:
                $dataCenterClass = CNDataCenter::class;
                break;
            case DataCenterLocation::AU:
                $dataCenterClass = AUDataCenter::class;
                break;
            default:
                $dataCenterClass = EUDataCenter::class;
                break;
        }

        $reflection = new ReflectionClass($dataCenterClass);

        $reflectionClassInstance = $reflection->newInstanceWithoutConstructor();

        $environment = $this->dataCenterEnvironment->value;

        return $reflectionClassInstance->{$environment}();
    }

    /**
     * @throws \ReflectionException
     */
    public static function instance(
        ?DataCenterLocation $dataCenterLocation = null,
        ?DataCenterEnvironment $dataCenterEnvironment = null
    ) {
        return (new self($dataCenterLocation, $dataCenterEnvironment))->setup();
    }
}