<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Core;

use com\zoho\crm\api\SDKConfigBuilder;
use com\zoho\crm\api\sdkconfigbuilder\SDKConfig;

class Config
{
    protected SDKConfig $config;

    public function __construct()
    {
        $this->config = (new SDKConfigBuilder())
            ->setAutoRefreshFields(false)
            ->setPickListValidation(true)
            ->setSSLVerification(false)
            ->connectionTimeout(2)
            ->timeout(2)
            ->build();
    }

    public static function instance(): SDKConfig
    {
        return (new self())->config;
    }
}