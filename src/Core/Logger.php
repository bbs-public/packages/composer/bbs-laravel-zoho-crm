<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Core;

use com\zoho\api\logger\Logger as ZohoSDKLogger;

class Logger
{
    public ZohoSDKLogger $instance;

    public function __construct(?string $level = null, ?string $path = null)
    {
        $this->instance = ZohoSDKLogger::getInstance(
            $level ?? config('zohocrm.logger.level'),
            $path ?? config('zohocrm.logger.path'),
        );
    }

    public static function instance(?string $level = null, ?string $path = null): ZohoSDKLogger
    {
        return (new self($level, $path))->instance;
    }
}