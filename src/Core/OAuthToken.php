<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Core;

use com\zoho\api\authenticator\OAuthToken as ZohoSDKOAuthToken;
use com\zoho\api\authenticator\TokenType;

class OAuthToken
{
    public ZohoSDKOAuthToken $token;

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public function __construct(?string $token = null)
    {
        if ($token) {
            $this->token = new ZohoSDKOAuthToken(
                config('zohocrm.client.client_id'),
                config('zohocrm.client.client_secret'),
                $token,
                TokenType::GRANT,
                config('zohocrm.client.redirect_url'),
            );
        }
    }

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public static function instance(string $token): ZohoSDKOAuthToken
    {
        return (new self($token))->token;
    }
}