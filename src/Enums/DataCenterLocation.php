<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static EU()
 * @method static US()
 * @method static INDIA()
 * @method static CN()
 * @method static AU()
 */
class DataCenterLocation extends Enum
{
    public const EU = 'EU';
    public const US = 'US';
    public const INDIA = 'IN';
    public const CN = 'CN';
    public const AU = 'AU';
}