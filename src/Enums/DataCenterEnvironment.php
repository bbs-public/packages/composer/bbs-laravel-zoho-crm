<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static PRODUCTION()
 * @method static DEVELOPER()
 * @method static SANDBOX()
 */
class DataCenterEnvironment extends Enum
{
    public const PRODUCTION = 'PRODUCTION';
    public const DEVELOPER = 'DEVELOPER';
    public const SANDBOX = 'SANDBOX';
}