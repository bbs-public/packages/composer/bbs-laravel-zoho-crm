<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static LEADS()
 * @method static CONTACTS()
 */
class ModuleType extends Enum
{
    public const LEADS = 'Leads';
    public const CONTACTS = 'Contacts';
}