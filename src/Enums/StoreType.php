<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Enums;

use BenSampo\Enum\Enum;

/**
 * @method static DATABASE()
 */
class StoreType extends Enum
{
    public const DATABASE = 'DATABASE';
}