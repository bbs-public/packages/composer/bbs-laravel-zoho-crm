<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Exceptions;

use Exception;

class MissingMandatoryFieldsException extends Exception
{
}