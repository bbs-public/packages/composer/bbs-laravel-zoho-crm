<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Contracts;

interface ModuleContract
{
    public function create(array $values);
}