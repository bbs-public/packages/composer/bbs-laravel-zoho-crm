<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Stores;

use BBSLab\ZohoCRM\Models\PersistedToken;
use com\zoho\api\authenticator\OAuthToken;
use com\zoho\api\authenticator\store\TokenStore;
use Illuminate\Database\Eloquent\Collection;

class EloquentStore implements TokenStore
{
    public function getClientId()
    {
        return config('zohocrm.client.client_id');
    }

    /**
     * @param  \com\zoho\crm\api\UserSignature  $user
     * @param  \com\zoho\api\authenticator\OAuthToken  $token
     * @return \com\zoho\api\authenticator\OAuthToken|null
     */
    public function getToken($user, $token): ?OAuthToken
    {
        $email = $user->getEmail();

        $clientId = $this->getClientId();

        /** @var PersistedToken $persistedToken */
        $persistedToken = PersistedToken::query()
            ->where('email', '=', $email)
            ->where('client_id', '=', $clientId)
            ->first();

        if ($persistedToken === null) {
            return null;
        }

        $token->setAccessToken($persistedToken->access_token);

        $token->setRefreshToken($persistedToken->refresh_token);

        $token->setExpiresIn($persistedToken->expires_at->getTimestampMs());

        return $token;
    }

    /**
     * @param  \com\zoho\crm\api\UserSignature  $user
     * @param  \com\zoho\api\authenticator\OAuthToken  $token
     */
    public function saveToken($user, $token): void
    {
        PersistedToken::query()
            ->updateOrCreate(
                [
                    'email' => $user->getEmail(),
                    'client_id' => $this->getClientId(),
                ],
                [
                    'access_token' => $token->getAccessToken(),
                    'refresh_token' => $token->getRefreshToken(),
                    'expires_at' => $token->getExpiresIn(),
                ]
            );
    }

    /**
     * @param  \com\zoho\api\authenticator\OAuthToken  $token
     */
    public function deleteToken($token): int
    {
        return PersistedToken::query()
            ->where('email', '=', $token->getUserMail())
            ->where('client_id', '=', $this->getClientId())
            ->delete();
    }

    public function getTokens(): Collection
    {
        return PersistedToken::query()
            ->where('client_id', '=', $this->getClientId())
            ->get();
    }

    public function deleteTokens(): int
    {
        return PersistedToken::query()
            ->where('client_id', '=', $this->getClientId())
            ->delete();
    }
}