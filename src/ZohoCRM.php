<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM;

use BBSLab\ZohoCRM\Core\Config;
use BBSLab\ZohoCRM\Core\Environment;
use BBSLab\ZohoCRM\Core\Logger;
use BBSLab\ZohoCRM\Core\Store;
use BBSLab\ZohoCRM\Core\OAuthToken;
use BBSLab\ZohoCRM\Core\UserSignature;
use BBSLab\ZohoCRM\Stores\EloquentStore;
use com\zoho\api\authenticator\OAuthToken as ZohoSDKOAuthToken;
use com\zoho\api\logger\Logger as ZohoSDKLogger;
use com\zoho\crm\api\dc\Environment as ZohoSDKEnvironment;
use com\zoho\crm\api\Initializer;
use com\zoho\crm\api\sdkconfigbuilder\SDKConfig as ZohoSDKConfig;
use com\zoho\crm\api\UserSignature as ZohoSDKUserSignature;
use Illuminate\Support\Str;

class ZohoCRM
{
    public ZohoSDKLogger $logger;
    public ZohoSDKUserSignature $userSignature;
    public ZohoSDKEnvironment $environment;
    public ZohoSDKOAuthToken $oauthToken;
    public EloquentStore $store;
    public ZohoSDKConfig $config;

    /**
     * @throws \ReflectionException
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public function __construct(string $grantToken)
    {
        $this->instanciateLogger();
        $this->instanciateUserSignature();
        $this->instanciateEnvironment();
        $this->instanciateOAuthToken($grantToken);
        $this->instanciateStore();
        $this->instanciateConfig();
    }

    public function instanciateLogger()
    {
        $this->logger = Logger::instance();
    }

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public function instanciateUserSignature()
    {
        $this->userSignature = UserSignature::instance();
    }

    /**
     * @throws \ReflectionException
     */
    public function instanciateEnvironment()
    {
        $this->environment = Environment::instance();
    }

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public function instanciateOAuthToken(string $token)
    {
        $this->oauthToken = OAuthToken::instance($token);
    }

    public function instanciateStore()
    {
        $this->store = Store::instance();
    }

    public function instanciateConfig()
    {
        $this->config = Config::instance();
    }

    public function resourcesPath(): string
    {
        return config('zohocrm.resources.path');
    }

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public function init(): ZohoCRM
    {
        Initializer::initialize(
            $this->userSignature,
            $this->environment,
            $this->oauthToken,
            $this->store,
            $this->config,
            $this->resourcesPath(),
            $this->logger,
        );

        return $this;
    }

    /**
     * @throws \ReflectionException
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public static function instance(?string $grantToken = null): ZohoCRM
    {
        return (new self($grantToken ?? Str::random()));
    }
}
