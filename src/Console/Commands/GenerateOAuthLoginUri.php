<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Console\Commands;

use BBSLab\ZohoCRM\Services\OAuthService;
use Illuminate\Console\Command;

class GenerateOAuthLoginUri extends Command
{
    protected $signature = 'zoho:auth';

    protected $description = 'Generates a link to authenticate a Zoho account and retrieve tokens';

    public function handle(): int
    {
        $uri = OAuthService::getAuthenticationUrl();

        $this->newLine();

        $this->comment('Open this url in your browser to complete the authentication process');

        $this->line('(tip: you can CMD+Click from your terminal to open the link)');

        $this->newLine();

        $this->info($uri);

        $this->newLine();

        return Command::SUCCESS;
    }
}