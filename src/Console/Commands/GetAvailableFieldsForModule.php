<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Console\Commands;

use BBSLab\ZohoCRM\Enums\ModuleType;
use BBSLab\ZohoCRM\Services\FieldsService;
use Illuminate\Console\Command;

class GetAvailableFieldsForModule extends Command
{
    protected $signature = 'zoho:fields {module}';

    protected $description = 'Sends a request to the connected Zoho CRM account to retrieve all the available fields for the given module';

    public function handle(): int
    {
        $module = ModuleType::fromValue($this->argument('module'));

        $fields = FieldsService::availableFieldsForModule($module);

        $fields->transform(function (array $field) {
            $field['pickListValues'] = implode(',', $field['pickListValues']);

            return $field;
        });

        $this->table(
            [
                'id',
                'api_Name',
                'systemMandatory',
                'mandatory',
                'fieldLabel',
                'displayLabel',
                'dataType',
                'pickListValues',
                'type',
            ],
            $fields,
        );

        return Command::SUCCESS;
    }
}