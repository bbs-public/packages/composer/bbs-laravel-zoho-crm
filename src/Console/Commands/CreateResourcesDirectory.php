<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Console\Commands;

use com\zoho\crm\api\util\Constants;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Storage;

class CreateResourcesDirectory extends Command
{
    protected $signature = 'zoho:resources';

    protected $description = 'Creates resources directories';

    public function handle(): int
    {
        $base = rtrim(
            str_replace(storage_path('app/'), '', config('zohocrm.resources.path')),
            '/'
        );

        $paths = [
            $base,
            $base.DIRECTORY_SEPARATOR.Constants::FIELD_DETAILS_DIRECTORY,
        ];

        foreach ($paths as $path) {
            if (!Storage::exists($path)) {
                Storage::makeDirectory($path);
            }
        }

        return Command::SUCCESS;
    }
}