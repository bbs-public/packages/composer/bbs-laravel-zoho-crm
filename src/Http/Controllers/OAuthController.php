<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Http\Controllers;

use BBSLab\ZohoCRM\Http\Requests\OAuthRequest;
use BBSLab\ZohoCRM\Services\OAuthService;
use com\zoho\crm\api\exception\SDKException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use ReflectionException;

class OAuthController extends Controller
{
    public function callback(OAuthRequest $request): JsonResponse
    {
        try {
            OAuthService::generateToken(request('code'));

            return response()->json([
                'status' => Response::HTTP_OK,
                'message' => 'Your Zoho CRM account was successfully authenticated',
            ]);
        } catch (ReflectionException | SDKException $e) {
            report($e);

            return response()->json([
                'status' => Response::HTTP_INTERNAL_SERVER_ERROR,
                'message' => 'There was an error authenticating your Zoho CRM account',
            ], Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}