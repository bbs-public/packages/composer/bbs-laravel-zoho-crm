<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OAuthRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'code' => ['required'],
        ];
    }
}