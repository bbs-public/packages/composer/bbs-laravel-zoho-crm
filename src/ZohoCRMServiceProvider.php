<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM;

use BBSLab\ZohoCRM\Console\Commands\ClearCache;
use BBSLab\ZohoCRM\Console\Commands\CreateResourcesDirectory;
use BBSLab\ZohoCRM\Console\Commands\GenerateOAuthLoginUri;
use BBSLab\ZohoCRM\Console\Commands\GetAvailableFieldsForModule;
use com\zoho\crm\api\util\Constants;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\ServiceProvider;

class ZohoCRMServiceProvider extends ServiceProvider
{
    public function boot(): void
    {
        $this->loadMigrationsFrom(__DIR__.'/../database/migrations');

        $this->loadRoutesFrom(__DIR__.'/../routes/web.php');

        if ($this->app->runningInConsole()) {
            $this->publishes(
                [
                    __DIR__.'/../config/zohocrm.php' => config_path('zohocrm.php'),
                ],
                'zohocrm-config'
            );

            $this->registerCommands();
        }
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/zohocrm.php', 'zohocrm');

        $this->app->singleton('zohocrm', function () {
            return new ZohoCRM('');
        });
    }

    public function registerCommands(): void
    {
        $this->commands([
            GenerateOAuthLoginUri::class,
            GetAvailableFieldsForModule::class,
            ClearCache::class,
            CreateResourcesDirectory::class,
        ]);
    }

    protected function ensureDirectoriesExist(): void
    {
        $base = rtrim(
            str_replace(storage_path('app/'), '', config('zohocrm.resources.path')),
            '/'
        );

        $paths = [
            $base,
            $base.DIRECTORY_SEPARATOR.Constants::FIELD_DETAILS_DIRECTORY,
        ];

        foreach ($paths as $path) {
            if (!Storage::exists($path)) {
                Storage::makeDirectory($path);
            }
        }
    }
}
