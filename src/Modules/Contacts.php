<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Modules;

use BBSLab\ZohoCRM\Concerns\BaseModule;
use BBSLab\ZohoCRM\Contracts\ModuleContract;
use BBSLab\ZohoCRM\Enums\ModuleType;

class Contacts extends BaseModule implements ModuleContract
{
    public string $moduleName = ModuleType::CONTACTS;
}