<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Services;

use BBSLab\ZohoCRM\Enums\ModuleType;
use BBSLab\ZohoCRM\ZohoCRM;
use com\zoho\crm\api\fields\Field;
use com\zoho\crm\api\fields\FieldsOperations;
use com\zoho\crm\api\fields\PickListValue;
use com\zoho\crm\api\ParameterMap;
use Illuminate\Support\Collection;

class FieldsService
{
    public static function availableFieldsForModule(ModuleType $module): Collection
    {
        ZohoCRM::instance()->init();

        $fieldsOperations = new FieldsOperations($module->value);

        $parametersInstance = new ParameterMap();

        $response = $fieldsOperations->getFields($parametersInstance);

        /** @var \com\zoho\crm\api\fields\ResponseWrapper $responseWrapper */
        $responseWrapper = $response->getObject();

        $fields = $responseWrapper->getFields();

        return collect($fields)
            ->filter()
            ->map(fn(Field $field) => self::mapField($field));
    }

    public static function mapField(Field $field): array
    {
        return [
            'id' => $field->getId(),
            'api_Name' => $field->getAPIName(),
            'systemMandatory' => $field->getSystemMandatory(),
            'mandatory' => $field->getMandatory(),
            'fieldLabel' => $field->getFieldLabel(),
            'displayLabel' => $field->getDisplayLabel(),
            'dataType' => $field->getDataType(),
            'pickListValues' => collect($field->getPickListValues())->map(fn(PickListValue $value
            ) => $value->getActualValue())->toArray(),
            'type' => $field->getDataType(),
        ];
    }
}