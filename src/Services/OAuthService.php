<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Services;

use BBSLab\ZohoCRM\ZohoCRM;
use com\zoho\api\authenticator\OAuthToken;
use com\zoho\api\authenticator\TokenType;
use JustSteveKing\UriBuilder\Uri;

class OAuthService
{
    public static function getAuthenticationUrl(): string
    {
        $host = config('zohocrm.oauth.host');

        $scopes = implode(',', config('zohocrm.oauth.scopes'));

        $clientId = config('zohocrm.client.client_id');

        $redirectUrl = config('zohocrm.client.redirect_url');

        $uri = Uri::build()
            ->addScheme('https')
            ->addHost($host)
            ->addPath('oauth/v2/auth')
            ->addQueryParam('scope', $scopes)
            ->addQueryParam('client_id', $clientId)
            ->addQueryParam('response_type', 'code')
            ->addQueryParam('access_type', 'offline')
            ->addQueryParam('prompt', 'consent')
            ->addQueryParam('redirect_uri', $redirectUrl);

        return $uri->toString();
    }

    /**
     * @throws \com\zoho\crm\api\exception\SDKException
     * @throws \ReflectionException
     */
    public static function generateToken(string $grantToken)
    {
        $token = new OAuthToken(
            config('zohocrm.client.client_id'),
            config('zohocrm.client.client_secret'),
            $grantToken,
            TokenType::GRANT,
            config('zohocrm.client.redirect_url'),
        );

        $instance = ZohoCRM::instance($grantToken)->init();

        $token->generateAccessToken($instance->userSignature, $instance->store);
    }
}