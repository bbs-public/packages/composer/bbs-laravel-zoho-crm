<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM;

use Illuminate\Support\Facades\Facade;

class ZohoCRMFacade extends Facade
{
    protected static function getFacadeAccessor(): string
    {
        return 'zohocrm';
    }
}
