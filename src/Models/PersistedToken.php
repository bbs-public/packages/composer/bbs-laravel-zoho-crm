<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property string $email
 * @property string $client_id
 * @property string $access_token
 * @property string $refresh_token
 * @property string $grant_token
 * @property Carbon $expires_at
 */
class PersistedToken extends Model
{
    public function getTable()
    {
        return config('zohocrm.stores.database.table_name');
    }

    protected $fillable = [
        'email',
        'client_id',
        'access_token',
        'refresh_token',
        'grant_token',
        'expires_at',
    ];

    protected $casts = [
        'expires_at' => 'datetime',
    ];

    protected $hidden = [
        'access_token',
        'refresh_token',
        'grant_token',
    ];

    public function setExpiresAtAttribute(?string $value = null): void
    {
        if ($value) {
            $this->attributes['expires_at'] = Carbon::createFromTimestampMs($value);
        }
    }
}