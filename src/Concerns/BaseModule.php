<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Concerns;

use BBSLab\ZohoCRM\ZohoCRM;
use com\zoho\crm\api\record\BodyWrapper;
use com\zoho\crm\api\record\Field;
use com\zoho\crm\api\record\Record;
use com\zoho\crm\api\record\RecordOperations;
use com\zoho\crm\api\util\APIResponse;

abstract class BaseModule
{
    public string $moduleName;

    public RecordOperations $recordOperations;

    public BodyWrapper $bodyWrapper;

    public Record $record;

    public Field $field;

    public array $records = [];

    /**
     * @throws \ReflectionException
     * @throws \com\zoho\crm\api\exception\SDKException
     */
    public function __construct()
    {
        ZohoCRM::instance()->init();

        $this->recordOperations = new RecordOperations();

        $this->bodyWrapper = new BodyWrapper();

        $this->record = new Record();
    }


    public array $fields = [];

    public function create(array $values): ?APIResponse
    {
        $this->map($values);

        array_push($this->records, $this->record);

        $this->bodyWrapper->setData($this->records);

        return $this->recordOperations->createRecords($this->moduleName, $this->bodyWrapper);
    }

    public function map(array $values)
    {
        foreach ($values as $key => $value) {
            $this->record->addKeyValue($key, $value);
        }
    }
}