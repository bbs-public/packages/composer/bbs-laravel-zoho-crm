<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Core\OAuthToken;
use BBSLab\ZohoCRM\Models\PersistedToken;
use BBSLab\ZohoCRM\Tests\TestCase;
use Illuminate\Support\Str;

uses(TestCase::class)->in(__DIR__);

expect()->extend('toBeValidUri', function () {
    return expect(parse_url($this->value))->toBeTruthy();
});

function testToken()
{
    return PersistedToken::query()
        ->firstOrCreate(
            [
                'email' => 'guillaume.tonet@neofa.com',
                'client_id' => '1000.792BLKKKWINUVT5VYBIW80X2W0DFXC',
            ],
            [
                'access_token' => '1000.74b3d0a63cf43a3ff299509ddaf1b0ff.0faf4946b635a7cee5d68b8c55e79312',
                'refresh_token' => '1000.ab50982ae976f0ad88ffbb681048072f.eb3052e315b056f8474aae146d0f195c',
                'expires_at' => '1633337305000',
            ],
        );
}

function oauthToken()
{
    return new OAuthToken(Str::random());
}