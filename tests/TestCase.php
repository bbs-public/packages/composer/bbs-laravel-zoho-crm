<?php

declare(strict_types=1);

namespace BBSLab\ZohoCRM\Tests;

use BBSLab\ZohoCRM\ZohoCRMServiceProvider;
use Illuminate\Foundation\Testing\LazilyRefreshDatabase;
use Orchestra\Testbench\TestCase as BaseTestCase;

class TestCase extends BaseTestCase
{
    use LazilyRefreshDatabase;

    protected function getPackageProviders($app): array
    {
        return [
            ZohoCRMServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set(
            'zohocrm.resources.path',
            './tests/storage/public/zoho/resources',
        );

        $app['config']->set('app.url', 'http://127.0.0.1:8000');
    }
}