<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Core\OAuthToken;
use com\zoho\api\authenticator\OAuthToken as ZohoSDKOAuthToken;
use Illuminate\Support\Str;

it('returns the correct token instance', function () {
    $token = OAuthToken::instance(Str::random());

    expect($token)->toBeInstanceOf(ZohoSDKOAuthToken::class);
});