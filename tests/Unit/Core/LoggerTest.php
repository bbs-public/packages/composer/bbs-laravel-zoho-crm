<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Core\Logger;
use com\zoho\api\logger\Logger as ZohoSDKLogger;

it('returns the correct logger instance', function () {
    $logger = Logger::instance();

    expect($logger)->toBeInstanceOf(ZohoSDKLogger::class);
});