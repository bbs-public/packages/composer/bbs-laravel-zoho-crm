<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Core\UserSignature;
use com\zoho\crm\api\UserSignature as ZohoSDKUserSignatureAlias;

it('returns the correct user signature instance', function () {
    $userSignature = UserSignature::instance();


    expect($userSignature)->toBeInstanceOf(ZohoSDKUserSignatureAlias::class);
});