<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Core\Environment;
use BBSLab\ZohoCRM\Enums\DataCenterEnvironment;
use BBSLab\ZohoCRM\Enums\DataCenterLocation;
use com\zoho\crm\api\dc\Environment as ZohoSDKEnvironment;

it('returns the correct data center for eu production', function () {
    $location = DataCenterLocation::EU();

    $environment = DataCenterEnvironment::PRODUCTION();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for eu developer', function () {
    $location = DataCenterLocation::EU();

    $environment = DataCenterEnvironment::DEVELOPER();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for eu sandbox', function () {
    $location = DataCenterLocation::EU();

    $environment = DataCenterEnvironment::SANDBOX();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for us production', function () {
    $location = DataCenterLocation::US();

    $environment = DataCenterEnvironment::PRODUCTION();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for us developer', function () {
    $location = DataCenterLocation::US();

    $environment = DataCenterEnvironment::DEVELOPER();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for us sandbox', function () {
    $location = DataCenterLocation::US();

    $environment = DataCenterEnvironment::SANDBOX();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});


it('returns the correct data center for in production', function () {
    $location = DataCenterLocation::INDIA();

    $environment = DataCenterEnvironment::PRODUCTION();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for in developer', function () {
    $location = DataCenterLocation::INDIA();

    $environment = DataCenterEnvironment::DEVELOPER();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for in sandbox', function () {
    $location = DataCenterLocation::INDIA();

    $environment = DataCenterEnvironment::SANDBOX();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for cn production', function () {
    $location = DataCenterLocation::CN();

    $environment = DataCenterEnvironment::PRODUCTION();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for cn developer', function () {
    $location = DataCenterLocation::CN();

    $environment = DataCenterEnvironment::DEVELOPER();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for cn sandbox', function () {
    $location = DataCenterLocation::CN();

    $environment = DataCenterEnvironment::SANDBOX();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for au production', function () {
    $location = DataCenterLocation::AU();

    $environment = DataCenterEnvironment::PRODUCTION();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for au sandbox', function () {
    $location = DataCenterLocation::AU();

    $environment = DataCenterEnvironment::SANDBOX();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});

it('returns the correct data center for au developer', function () {
    $location = DataCenterLocation::AU();

    $environment = DataCenterEnvironment::DEVELOPER();

    $dataCenter = Environment::instance($location, $environment);

    expect($dataCenter)->toBeInstanceOf(ZohoSDKEnvironment::class);
});