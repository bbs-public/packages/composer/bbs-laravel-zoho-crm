<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Services\OAuthService;

test('it returns an url to authenticate a zoho account', function () {
    $url = OAuthService::getAuthenticationUrl();

    expect($url)->toBeValidUri();
});
