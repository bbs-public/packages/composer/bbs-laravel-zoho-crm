<?php

declare(strict_types=1);

it('runs a command to generate an oauth login uri', function () {
    testToken();

    $this->artisan('zoho:auth')
        ->assertExitCode(0);
});