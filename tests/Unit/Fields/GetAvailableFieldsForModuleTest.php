<?php

declare(strict_types=1);

it('runs a command to generate an oauth login uri', function () {
    testToken();

    $this->artisan('zoho:fields Leads')
        ->assertExitCode(0);
});