<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Exceptions\MissingMandatoryFieldsException;
use BBSLab\ZohoCRM\Modules\Leads;

it('creates a record for leads modules', function () {
    testToken();

    $values = [
        'Last_Name' => 'John Doe Test',
    ];

    $response = (new Leads())->create($values);

    expect($response->getStatusCode())->toEqual(201);
});
