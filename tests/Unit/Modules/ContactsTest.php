<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Exceptions\MissingMandatoryFieldsException;
use BBSLab\ZohoCRM\Modules\Contacts;

it('creates a record for contacts modules', function () {
    testToken();

    $values = [
        'Last_Name' => 'John Doe Test',
    ];

    $response = (new Contacts())->create($values);

    expect($response->getStatusCode())->toEqual(201);
});
