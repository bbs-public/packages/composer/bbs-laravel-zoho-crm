<?php

declare(strict_types=1);

use BBSLab\ZohoCRM\Core\UserSignature;
use BBSLab\ZohoCRM\Stores\EloquentStore;

it('deletes a token from eloquent store', function () {
    $oauthToken = oauthToken();
    $user = UserSignature::instance();

    $oauthToken->token->setUserMail($user->getEmail());

    $store = new EloquentStore();

    $store->saveToken($user, $oauthToken->token);

    expect($store->deleteToken($oauthToken->token))->toBeTruthy();
});

it('retrives all eloquent store entries for the current client id', function () {
    $oauthToken = oauthToken();

    $user = UserSignature::instance();

    $oauthToken->token->setUserMail($user->getEmail());

    $store = new EloquentStore();

    $store->saveToken($user, $oauthToken->token);

    $tokens = $store->getTokens();

    expect($tokens)->toHaveCount(1);
});

it('deletes all eloquent store entries for the current client id', function () {
    $oauthToken = oauthToken();

    $user = UserSignature::instance();

    $oauthToken->token->setUserMail($user->getEmail());

    $store = new EloquentStore();

    $store->saveToken($user, $oauthToken->token);

    $store->deleteTokens();

    $tokens = $store->getTokens();

    expect($tokens)->toHaveCount(0);
});